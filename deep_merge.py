# -*- coding: utf-8 -*-
from copy import deepcopy


def deep_merge(first, second):
    """
    >>> a = {'one': 1, 'two': {'three': 3, 'five': {'six': 6}}}
    >>> b = {'one': '1', 'two': {'four': 4, 'five': {'seven': 7}}}
    >>> deep_merge(a, b) == {
    ... 'one': 1, 'two': {'three': 3, 'four': 4, 'five': {'six': 6, 'seven': 7}}
    ... }
    True
    """
    res = dict(second)
    for k, v in first.iteritems():
        if isinstance(v, dict) and isinstance(second.get(k), dict):
            res[k] = deep_merge(v, second[k])
        else:
            res[k] = v
    return res


def deep_merge_nr(first, second):
    """
    >>> a = {'one': 1, 'two': {'three': 3, 'five': {'six': 6}}}
    >>> b = {'one': '1', 'two': {'four': 4, 'five': {'seven': 7}}}
    >>> deep_merge_nr(a, b) == {
    ... 'one': 1, 'two': {'three': 3, 'four': 4, 'five': {'six': 6, 'seven': 7}}
    ... }
    True
    """
    tmp = deepcopy(second)
    result = deepcopy(first)
    stack = [(tmp, result)]
    while tmp:
        try:
            k, v = tmp.iteritems().next()
        except StopIteration:
            break
        if not isinstance(v, dict):
            i = -1
            while not stack[i][1]:
                i -= 1
            stack[i][1].setdefault(k, tmp.pop(k))
        elif isinstance(v, dict) and not isinstance(stack[-1][1].get(k), dict):
            stack[-1][1].setdefault(k, tmp.pop(k))
        elif not v:
            tmp.pop(k)
        else:
            stack.append((v, stack[-1][1].get(k) if stack[-1][1] else None))
        if stack and not stack[-1][0]:
            stack.pop()
        if stack:
            tmp = stack[-1][0]
    return result


def deep_merge_nr1(first, second):
    """
    >>> a = {'one': 1, 'two': {'three': 3, 'five': {'six': 6}}}
    >>> b = {'one': '1', 'two': {'four': 4, 'five': {'seven': 7}}}
    >>> deep_merge_nr1(a, b) == {
    ... 'one': 1, 'two': {'three': 3, 'four': 4, 'five': {'six': 6, 'seven': 7}}
    ... }
    True
    """
    result = deepcopy(second)
    iterator = first.iteritems()
    link = result
    iter_stack = []
    while True:
        try:
            while True:
                k, v = iterator.next()
                if isinstance(v, dict) and isinstance(link.get(k), dict):
                    iter_stack.append((iterator, link))
                    iterator = v.iteritems()
                    link = link[k]
                    break
                else:
                    link.update({k: v})
        except StopIteration:
            if iter_stack:
                iterator, link = iter_stack.pop()
            else:
                break
    return result


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
