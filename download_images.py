# -*- coding: utf-8 -*-
import os
import sys
import struct
import urllib2


class Data(object):
    
    def __init__(self, string):
        self._value = string
    
    def __len__(self):
        return len(self._value)
    
    def __getslice__(self, i, j):
        return self._value[i:j]
    
    def __eq__(self, item):
        return self._value == item
    
    def __getitem__(self, index):
        return self._value[index]
    
    def __str__(self):
        return self._value
    
    def add_last(self, string):
        self._value += string
        return string
    
    def startswith(self, string):
        return self._value.startswith(string)


def get_image_info(url):
    request = urllib2.Request(url)
    try:
        response = urllib2.urlopen(request)
    except (urllib2.URLError, ValueError), e:
        print '%s downloading error, %s' %(url, e)
        return None
    data = Data(response.read(6))
    #GIF header
    if data.startswith('GIF'):
        return {
            'type': 'gif',
            'size': struct.unpack('<HH', data.add_last(response.read(4)))
        }
    #PNG header
    data.add_last(response.read(2))
    if data == '\x89\x50\x4E\x47\x0D\x0A\x1A\x0A':
        l, chunk_type = struct.unpack('>L4s', data.add_last(response.read(8)))
        #looking for IHDR chunk
        while chunk_type != 'IHDR':
            data.add_last(response.read(l+4))
            l, chunk_type = struct.unpack(
                '>L4s', data.add_last(response.read(8))
            )
        return {
            'type': 'png',
            'size': struct.unpack('>LL', data.add_last(response.read(8)))
        }
    #JPEG header
    if data.startswith('\xFF\xD8'):
        i = 2
        j = 0
        marker = data[i:i+2]
        #looking for SOFn Baseline DCT
        while marker != '\xFF\xDA':
            marker_len = struct.unpack('>H', data[i+2:i+4])[0]
            if marker_len+i+2 > len(data):
                data.add_last(response.read(marker_len+i+6-len(data)))
            if marker[-1:] in ('\xC0', '\xC1', '\xC2', '\xC3'):
                return {
                    'type': 'jpeg',
                    'size': struct.unpack('>HH', data[i+5:i+9])[::-1]
                }
            i = marker_len+i+2
            marker = data[-4:-2]
    return None


def available_images(filename, lim=None):
    lim = int(lim) if lim and lim.isdigit() else None
    if not os.path.isfile(filename):
        print '%s doesn`t exists' % filename
        return
    with open(filename, 'r') as f:
        for n, line in enumerate(f.readlines()):
            if lim and n == int(lim): break
            img_info = get_image_info(line.strip())
            if (
                img_info and
                img_info['size'][0]*img_info['size'][1]%4 == 0 and
                int(img_info['size'][0]*0.333333) > 0 and
                int(img_info['size'][1]*0.333333) > 0
            ):
                print line.strip()


if __name__ == '__main__':
    if len(sys.argv) == 2:
        available_images(sys.argv[1])
    elif len(sys.argv) == 3:
        available_images(*sys.argv[1:])
    else:
        print 'usage download_images.py <filename> <number of lines> (optional)'
